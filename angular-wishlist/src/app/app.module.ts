import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule, Validators, ValidatorFn } from '@angular/forms';
import { ActionReducerMap, StoreModule as NgRxStoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppComponent } from './app.component';

import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesEffects, DestinosViajesState, initialiazeDestinosViajesState, reducerDestinoViajes } from './models/destinos-viajes-state.model';


const routes: Routes = [
  { path : '', redirectTo: 'home', pathMatch: 'full'},
  { path : 'home', component: ListaDestinosComponent},
  { path : 'destino', component: DestinoDetalleComponent},
];

// REDUX INIT
export interface AppState {
  destinos: DestinosViajesState;
}
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinoViajes 
};

let reducersInitialState = {
  destinos: initialiazeDestinosViajesState()
};
 

// FIN REDUX

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument ()
  ],
  providers:  [
    DestinosApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
