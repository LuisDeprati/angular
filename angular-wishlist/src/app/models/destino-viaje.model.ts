export class DestinoViaje {  
  selected : boolean;
  servicios : string[];
  //id = uuid();
  constructor(public nombre: string, public url: string, public votes: number = 0) { 
    this.servicios = ['pileta','desayuno'];
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected (s: boolean) {
    this.selected = s;
  }
  VoteUp() {
    this.votes++;
  }
  VoteDown() {
    this.votes--;
  }
}
